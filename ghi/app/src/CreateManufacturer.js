import React, { useState } from 'react';

function ManufacturerForm() {
    const [name, setName] = useState('');

    const handleName = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.name = name;

        const salespersonUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {

            setName('');

        }
    }


    return (<div className="container">
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleName} value={name} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="first_name">Name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    )

}

export default ManufacturerForm
