import React, { useEffect, useState } from 'react';

function SalespeopleList() {

    const [salespeople, setSalespeople] = useState([]);

    async function getSalespeople() {
        const salespersonUrl = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(salespersonUrl)
        if (response.ok) {
            const { salespeople } = await response.json();
            setSalespeople(salespeople)
        } else {
            console.error('Error')
        }
    }

    useEffect(() => {
        getSalespeople();
    }, []);

    return (
        <div className="salespeople-list">
            <div className="salespeople-details" key={salespeople.href}>
                < table className="table table-striped" >
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Employee ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        {salespeople.map((salesperson) => {
                            return (
                                <tr key={salesperson.href}>
                                    <td>{salesperson.first_name}</td>
                                    <td>{salesperson.last_name}</td>
                                    <td>{salesperson.employee_id}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div >
    )
}

export default SalespeopleList
