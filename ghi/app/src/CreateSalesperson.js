import React, { useEffect, useState } from 'react';

function SalespersonForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value)
    }
    const handleLastName = (event) => {
        const value = event.target.value;
        setLastName(value)
    }
    const handleEmployeeId = (event) => {
        const value = event.target.value;
        setEmployeeId(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId

        const salespersonUrl = 'http://localhost:8090/api/salespeople/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {

            setFirstName('');
            setLastName('');
            setEmployeeId('');

        }
    }

    // useEffect(() => {
    //     fetchData();
    // }, [])

    return (<div className="container">
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Salesperson</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstName} value={firstName} placeholder="First Name" required type="text" id="first_name" name="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastName} value={lastName} placeholder="Last Name" required type="text" id="last_name" name="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeId} value={employeeId} placeholder="Employee Id" required type="text" id="employee_id" name="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    )

}

export default SalespersonForm
