import React, { useEffect, useState } from 'react';

function VehicleModelForm() {
    const [name, setName] = useState('');
    const [manufacturers, setManufacturers] = useState([]);
    const [chooseManufacturer, setChooseManufacturer] = useState('');
    const [picture, setPicture] = useState('');

    const handleName = (event) => {
        const value = event.target.value;
        setName(value)
    }
    const handleChooseManufacturer = (event) => {
        const value = event.target.value;
        setChooseManufacturer(value)
    }
    const handlePicture = (event) => {
        const value = event.target.value;
        setPicture(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.name = name;
        data.manufacturer_id = chooseManufacturer;
        data.picture_url = picture

        const modelsUrl = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(modelsUrl, fetchConfig);
        if (response.ok) {

            setName('');
            setChooseManufacturer('');
            setPicture('');

        }
    }

    const fetchData = async () => {
        try {
            const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'

            const response = await fetch(manufacturerUrl);

            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers)
            }
        }
        catch (error) {
            console.error('Error fetching data:', error)
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (<div className="container">
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Vehicle Model</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleName} value={name} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="chooseCustomer" className="form-label">Manufacturer</label>
                            <select onChange={handleChooseManufacturer} required id="chooseManufacturer" name="chooseManufacturer" className="form-select">
                                <option value="">Choose a Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePicture} value={picture} placeholder="Picture" required type="text" id="picture" name="picture" className="form-control" />
                            <label htmlFor="picture">Picture URL</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    )

}

export default VehicleModelForm
