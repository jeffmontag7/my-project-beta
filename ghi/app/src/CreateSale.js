import React, { useEffect, useState } from 'react';

function SaleForm() {

    const [automobile, setAutomobile] = useState([]);
    const [chooseAutomobile, setChooseAutomobile] = useState('');
    const [salesperson, setSalesperson] = useState([]);
    const [chooseSalesperson, setChooseSalesperson] = useState('');
    const [customer, setCustomer] = useState([]);
    const [chooseCustomer, setChooseCustomer] = useState('');
    const [price, setPrice] = useState('');

    const handleChooseAutomobile = (event) => {
        const value = event.target.value;
        setChooseAutomobile(value)
    }

    const handleChooseSalesperson = (event) => {
        const value = event.target.value;
        setChooseSalesperson(value)
    }

    const handleChooseCustomer = (event) => {
        const value = event.target.value;
        setChooseCustomer(value)
    }

    const handlePrice = (event) => {
        const value = event.target.value;
        setPrice(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
        };
        data.automobile = chooseAutomobile;
        data.salesperson = chooseSalesperson;
        data.customer = chooseCustomer;
        data.price = price;
        console.log(data);

        const saleUrl = 'http://localhost:8090/api/sale/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {


            setChooseAutomobile('');
            setChooseSalesperson('');
            setChooseCustomer('');
            setPrice('');

        }

    }
    const fetchData = async () => {
        try {
            const automobileUrl = 'http://localhost:8100/api/automobiles/';
            const automobileResponse = await fetch(automobileUrl);

            if (automobileResponse.ok) {
                const automobileData = await automobileResponse.json();
                const unsold = automobileData.autos.filter(auto => !auto.sold);
                setAutomobile(unsold);
            } else {
                console.log('Failed to fetch automobiles');
            }

            const salespersonUrl = 'http://localhost:8090/api/salespeople/';

            const salespersonResponse = await fetch(salespersonUrl);

            if (salespersonResponse.ok) {
                const salespersonData = await salespersonResponse.json();
                setSalesperson(salespersonData.salespeople)
            }
            const customerUrl = 'http://localhost:8090/api/customers/';

            const customerResponse = await fetch(customerUrl);

            if (customerResponse.ok) {
                const customerData = await customerResponse.json();
                setCustomer(customerData.customer)
            }
        }
        catch (error) {
            console.error('Error fetching data:', error);
        }
    }




    useEffect(() => {
        fetchData();
    }, []);


    return (<div className="container">
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Sale</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="mb-3">
                            <label htmlFor="chooseAutomobile" className="form-label">Automobile</label>
                            <select onChange={handleChooseAutomobile} required id="chooseAutomobile" name="chooseAutomobile" className="form-select">
                                <option value="">Choose an Automobile</option>
                                {automobile.map(automobile => {
                                    return (
                                        <option key={automobile.href} value={automobile.href}>
                                            {automobile.model.name} {automobile.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="chooseSalesperson" className="form-label">Salesperson</label>
                            <select onChange={handleChooseSalesperson} required id="chooseSalesperson" name="chooseSalesperson" className="form-select">
                                <option value="">Choose a Salesperson</option>
                                {salesperson.map(salespeople => {
                                    return (
                                        <option key={salespeople.employee_id} value={salespeople.employee_id}>
                                            {salespeople.employee_id}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="chooseCustomer" className="form-label">Customer</label>
                            <select onChange={handleChooseCustomer} required id="chooseCustomer" name="chooseCustomer" className="form-select">
                                <option value="">Choose a Customer</option>
                                {customer.map(customer => {
                                    return (
                                        <option key={customer.phone_number} value={customer.phone_number}>
                                            {customer.first_name} {customer.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePrice} value={price} placeholder="Price" required type="number" id="price" name="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    )

}

export default SaleForm;
