# CarCar
CarCar is your dealership's personal assistant, custom-built to make handling inventory, sales, and services a breeze.

Developers:

* Jaron Laquindanum - Services
* Jeff Montag - Sales

## Instructions

This application is built on a distributed architecture, leveraging multiple microservices. Docker played a key role in our development environment, enabling the containerization of services, pollers, React components, and the database. On the backend, we rely on Python with Django framework version 4.0.3, while React takes care of the frontend.

### Installing Docker through the terminal:

* Windows: winget install Docker.DockerDesktop

* Mac: brew install --cask docker

### Installing Git through the terminal:

* Windows: winget install Git.Git

* Mac: Install through: https://brew.sh/ then run the command (brew install git)

### Installing Python:
* https://www.python.org/downloads/

### Installing Django through pip:
* pip install django

### Forking the repository:
Copy the cloned repository then clone it to your local machine in your desired directory:
git clone <<your.repository.url.here>>

### Build and run the project in their containers using the Docker commands:

* docker volume create beta-data

* docker-compose build

* docker-compose up

When all your containers are running, you can view the project in your web browser at 'http://localhost:3000/'

## Diagram

https://excalidraw.com/#json=mo2DcPwJr7fOc50EBM-TH,6CiIUHthlSYL4ct_PW8v2Q

## API Documentation
Inventory Management: 

* This microservice handles tracking and storing the dealership's inventory of automobiles, including details like models and manufacturers.

Sales Tracking: 

* 

Services Management: 

* This microservice allows the dealership to schedule appointments and track their status, from creation to completion or cancellation. It syncs with the inventory API to manage information between inventory and the service department. Appointments include details like automobile VIN, customer information, technician assignment, date/time, VIP status, and service status. You can add appointments, technicians, view a list of appointments, and search for specific service appointments.

## URLs and Ports

### Services
| Action                           | Method | URL                                           |
|----------------------------------|--------|-----------------------------------------------|
| List technicians                 | GET    | http://localhost:8100/api/technicians/       |
| Create a technician              | POST   | http://localhost:8100/api/technicians/       |
| Delete a specific technician     | DELETE | http://localhost:8100/api/technicians/:id/   |
| List appointments               | GET    | http://localhost:8100/api/appointments/      |
| Create an appointment           | POST   | http://localhost:8100/api/appointments/      |
| Delete an appointment          | DELETE | http://localhost:8100/api/appointments/:id/  |
| Set appointment status to canceled | PUT    | http://localhost:8100/api/appointments/:id/cancel/ |
| Set appointment status to finished | PUT    | http://localhost:8100/api/appointments/:id/finish/ |

### Automobile Information

| Action                           | Method | URL                                            |
|----------------------------------|--------|------------------------------------------------|
| List automobiles                 | GET    | http://localhost:8100/api/automobiles/        |
| Create an automobile             | POST   | http://localhost:8100/api/automobiles/        |
| Get a specific automobile       | GET    | http://localhost:8100/api/automobiles/:vin/   |
| Update a specific automobile    | PUT    | http://localhost:8100/api/automobiles/:vin/   |
| Delete a specific automobile    | DELETE | http://localhost:8100/api/automobiles/:vin/   |

### Manufacturers
| Action                   | Method | URL                                   |
|--------------------------|--------|---------------------------------------|
| List manufacturers       | GET    | http://localhost:8100/api/manufacturers/ |
| Create a manufacturer    | POST   | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET    | http://localhost:8100/api/manufacturers/:id/ |
| Update a specific manufacturer | PUT    | http://localhost:8100/api/manufacturers/:id/ |
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/ |

## Service microservice

The services microservice makes it hassle-free for dealerships to schedule customer appointments for vehicle maintenance. Using a simple appointment creation form, users can easily input details like appointment date/time, status (created, finished, canceled), assigned technician, vehicle VIN, customer name, and VIP status. In the appointment list, users can quickly update the status to 'Canceled' or 'Finished' with just one click. Vehicle VIN is retrieved from the Automobile Value Object. Additionally, users can check the service history of any vehicle by its VIN.

Furthermore, users can access a comprehensive list of all technicians employed by the dealership and add new technicians through a dedicated form.

## Sales microservice

URLS AND PORTS:

Home: http://localhost:3000/
Manufacturers List: http://localhost:3000/manufacturers
Create a Manufacturer: http://localhost:3000/create/manufacturers
Models: http://localhost:3000/vehicle
Create a Model: http://localhost:3000/create/vehicle
Add a Salesperson: http://localhost:3000/create/salesperson
Add a Customer: http://localhost:3000/create/customer
Salespeople List: http://localhost:3000/salespeople
Customers List: http://localhost:3000/customers
Add a Sale: http://localhost:3000/create/sale
Sale List: http://localhost:3000/sales
Salesperson History: http://localhost:3000/sales/history

The sales microservice allows you to:
    create a salesperson, customer, and sale.
    view a list of all salespeople, customers, and sales
    view a historical list of all sales filtered by salesperson

The sales microservice is composed of 4 models: Automobile VO, Customer, Salesperson, and Sale.

AutomobileVO is a value object that uses a poller to get its data from the Inventory App.
Sale is a model that includes the other 3 models as foreign keys.

When creating a sale, we need to know what vehicle was sold (automobileVO), who sold it (salesperson), who bought it (customer), and the price. Since the vehicle data is in the inventory App that is why we need a poller to constantly update the sale app with recent data.

ENDPOINTS:

GET, POST, DELETE methods are included for the Customer, Salesperson, and Sale Models. AutomobileVO is not interacted with in the Sales microservice and instead is handled in the Inventory microservice.

The following are the endpoints related to each of the models

Customer: http://localhost:8090/api/customers/

GET request:
Will return you data in insomnia in the following format:

{
	"customer": [
		{
			"href": "/api/customers/3/",
			"first_name": "asdf",
			"last_name": "asdf",
			"address": "asdf",
			"phone_number": "1112223456"
		}
	]
}

POST request:
Complete a Post using the following format:
{
	"first_name": "Jeff",
	"last_name": "Montag",
	"address": "86 Cinnamon Teal",
	"phone_number": "1112223456"
}

DELETE request:
Simply send the request to http://localhost:8090/api/customers/id/. The ID is the digit in the href provided by your get request. You will be given a message of:
{
	"deleted": true
}
when the request successfully deletes data.

Salespeople: http://localhost:8090/api/salespeople/

GET request:
Will return you data in insomnia in the following format:

{
	"salespeople": [
		{
			"href": "/api/salespeople/1/",
			"first_name": "Jeff",
			"last_name": "Montag",
			"employee_id": "1"
		}
	]
}

POST request:
Complete a Post using the following format:

{
  "first_name": "Someone",
  "last_name": "New",
  "employee_id": "Snew"
}

DELETE request:
Simply send the request to http://localhost:8090/api/salespeople/{employee_id}/. You will be given a message of:
{
	"deleted": true
}
when the request successfully deletes data.

Sale: http://localhost:8090/api/sale/

GET request:
Will return you data in insomnia in the following format:

{
	"sale": [
		{
			"href": "/api/sale/3/",
			"automobile": {
				"import_href": "/api/automobiles/4Y1SL65848Z411430/",
				"vin": "4Y1SL65848Z411430",
				"sold": true
			},
			"salesperson": {
				"href": "/api/salespeople/1/",
				"first_name": "Jeff",
				"last_name": "Montag",
				"employee_id": 1
			},
			"customer": {
				"href": "/api/customers/3/",
				"first_name": "asdf",
				"last_name": "asdf",
				"address": "asdf",
				"phone_number": "1112223456"
			},
			"price": 10000
		}
	]
}

POST request:
Complete a Post using the following format:

{
	"automobile": "/api/automobiles/4Y1SL65848Z411430/",
	"salesperson": 1,
	"customer": "1112223456",
	"price": 10000
}

The foreign keys are correctly inputted by including the following:
automobile.href
salesperson.employee_id
customer.phone_number

In order to successfully make a sale, you must have an automobile in the Inventory Database. This automobile is dependent on having a vehicle model, which is in turn dependent on having a manufacturer. So, in order to make a sale make sure a manufacturer, then vehicle model, then automobile is created (in that order).

Price is just a decimal number with a max of 110 digits and 2 decimal places.

DELETE request:

Simply send the request to http://localhost:8090/api/sale/id/. The ID is the digit in the href provided by your get request. You will be given a message of:
{
	"deleted": true
}
when the request successfully deletes data.

Explain your models and integration with the inventory
microservice, here.
